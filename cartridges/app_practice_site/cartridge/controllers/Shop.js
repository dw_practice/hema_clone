// Importing scrips
var app = require('*/cartridge/scripts/app');
var guard = require('*/cartridge/scripts/guard');
    //Construct url's
var URLutils = require('dw/web/URLUtils');
var catalogMgr = require('dw/catalog/CatalogMgr');
var urlUtils = require('dw/web/URLUtils');

function show(){
    var params = request.getHttpParameterMap();
    var category = app.getModel('Category').get(params.category.toString());
    var categories = category.object.getSubCategories().toArray();
    var subCategory = params.subcategory.toString() || categories[0].displayName.toLowerCase();
    var subSubCategory = params.subsubcategory.toString();
    var products = getSubcategoriesProducts(categories, subCategory,subSubCategory);

    app.getView({
        params: params,
        subCategory: subCategory,
        categories: categories,
        products: products,
        urlUtils: urlUtils
    }).render('shop/shop');
}

function getSubcategoriesProducts(categories, wantedCategory, wantedSubCategory) {
    var result = [];
        wantedcategory = wantedCategory || categories[0].displayName.toLowerCase();

    for(var i = 0; i < categories.length; i++) {
        if(categories[i].displayName.toLowerCase() === wantedcategory)
        {   
            var subcategories = categories[i].getSubCategories().toArray();
            for(var j = 0; j < subcategories.length; j++) {
                if(wantedSubCategory) {
                    if(subcategories[j].displayName.toLowerCase() === wantedSubCategory){
                        result = result.concat(subcategories[j].getProducts().toArray());
                        break;
                    }

                } else {
                    result = result.concat(subcategories[j].getProducts().toArray());
                }
            }

            break;
        }
    }

    return result;
}

exports.Show = guard.ensure(['get'], show);