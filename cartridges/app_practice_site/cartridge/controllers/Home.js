/* Script Modules */
var app = require('*/cartridge/scripts/app');
var guard = require('*/cartridge/scripts/guard');

function show(){
    debugger;
    var category = app.getModel('Category');
    var menCategory = category.get('mens-clothing');
    // you used wrong method : its getSubcategories not subCategories !
    var mensSubcategories = menCategory.object.getSubCategories().toArray();
    app.getView({
        category: category,
        mensSubcategories: mensSubcategories,
        app: app
    }).render('content/home/homepage');
}

exports.Show = guard.ensure(['get'], show);